<!-- This is home.ctp view -->
<div class="container">

	<!-- C:\wamp\www\letsbuyit123\app\views\projects\home.ctp -->
		
	<?php
		if (!empty($project))
		{
			?>
			<div id="search-results">
			<h4>Featured Project</h4>
				<div id="search-items">
					<?php
					echo $this->element('projects/search-item', array('project'=>$project));
					?>
				</div>
			</div>
		<?php
		} else {
		?>
			<h4>Welcome to LetsBuyIt123</h4>
			<p>Nulla consectetur sem dolor, semper gravida urna hendrerit vitae. Quisque ac sagittis nisl, quis dignissim eros. In sit amet turpis sodales, tincidunt neque ut, tempus odio. Suspendisse posuere blandit placerat.
			</p>
			<p>
			Quisque nec pharetra tellus, id convallis ipsum. Sed ut commodo odio. Sed venenatis elementum velit, ac lacinia nisl blandit eget. Vivamus ullamcorper egestas orci varius mattis. Aliquam a consectetur nisi. Aliquam turpis est, condimentum a cursus eu, eleifend volutpat tellus. 
			</p>

			<div class="row">
			  <div class="col-md-4">
			    <div class="thumbnail">
			      <a href="projects/category/7">
			        <img src="img/greyhounds.jpg" alt="Greyhounds" style="width:100%">
			        <div class="caption">
			          <p>Greyhounds</p>
			        </div>
			      </a>
			    </div>
			  </div>
			  <div class="col-md-4">
			    <div class="thumbnail opaque">
			      <!-- <a href="#"> -->
			        <img src="img/campervans.jpg" alt="Camper Vans" style="width:100%">
			        <div class="caption">
			          <p>Camper Vans</p>
			        </div>
			      <!-- </a> -->
			    </div>
			  </div>
			  <div class="col-md-4">
			    <div class="thumbnail opaque">
			      <!-- <a href="#"> -->
			        <img src="img/boats.jpg" alt="Boats" style="width:100%">
			        <div class="caption">
			          <p>Boats</p>
			        </div>
			      <!-- </a> -->
			    </div>
			  </div>
			</div>
		<?php
			//echo $html->link( 'View Greyhounds', '/projects/category/7');
		}		
	?>

</div>
