<div class="container">
	<div id="search-results">
		<h4>Current Proposals</h4>
		<div id="search-filters">
			<ul class="breadcrumb">
				<li>Quick Links</li>
				
				<? $i=0; ?>
				
				<? foreach ($projectCategories as $catId=>$category): ?>
					<? if ($i != (count($projectCategories)-1)): ?>
						<li><a href="<?=HTTP_BASE ?>projects/category/<?=$catId?>"><?=$category?></a></li>
					<? else: ?>
						<li><a href="<?=HTTP_BASE ?>projects/category/<?=$catId?>"><?=$category?></a></li>
					<? endif; ?>
					<? $i++; ?>
				<? endforeach; ?>
			</ul>
		</div>
		<div class="clear"></div>
		<div id="search-items">
			<!-- <ul> -->
				<? foreach ($projects as $project) { ?>

				<?php echo $this->element('projects/search-item', array('project'=>$project)); ?>	
				
				<? } ?>
			<!-- </ul> -->
		</div> <!-- search-items -->
	
			<?php echo $this->element('projects/paginator'); ?>	

	</div> <!-- search-results -->
</div>