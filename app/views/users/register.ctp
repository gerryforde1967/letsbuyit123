<div class="container">

<div id="edit-form">

<div class="panel panel-default">
  <div class="panel-body" class="form-group">

  <h3>Register</h3>

<? if ($success){ ?>
<p></p>
<? } else { ?>
<?php echo $form->create('User', array( 'url' => 'register')); ?>
<dl>
<!--
<div class="field-hint-inactive" id="data[User][fname]-H"><div><?=TT_USER_FNAME?></div></div>
<?= $form->input('User.fname', array('label'=>'First Name')) ?>
-->
<?= $form->input('User.fname', array('class' => 'form-control', 'label' => 'First Name')) ?>
<!-- </dl>
<dl> -->

<!--
<div class="field-hint-inactive" id="data[User][lname]-H"><div><?=TT_USER_LNAME?></div></div>
<?= $form->input('User.lname', array('label'=> 'Last Name')); ?>
-->
<?= $form->input('User.lname', array('class' => 'form-control', 'label' => 'Last Name')) ?>

<!-- </dl>
<dl> -->
<!--<div class="field-hint-inactive" id="data[User][email]-H"><div><?=TT_USER_EMAIL?></div></div>
<?= $form->input('User.email', array('label'=> 'Email')); ?>-->
<?= $form->input('User.email', array('class' => 'form-control', 'label' => 'Email')) ?>
<!-- </dl>
<dl> -->

<!-- 
<div class="field-hint-inactive" id="data[User][password]-H"><div><?=TT_USER_PASSWORD?></div></div>
<?= $form->input('User.password') ?>
 -->

<?= $form->input('User.password', array('class' => 'form-control', 'label' => 'Password', 'type' => 'password')) ?>

<!-- </dl>
<dl> -->

<!-- 
<div class="field-hint-inactive" id="data[User][password_confirm]-H"><div><?=TT_USER_PASSWORD_CONFIRM?></div></div>
<?= $form->input('User.password_confirm', array( 'type' => 'password' ) ) ?>
-->
<?= $form->input('User.password_confirm', array('class' => 'form-control', 'type' => 'password' ) ) ?>

<!-- 
</dl>
<dl>
 -->

User Type:
<span id="demo"> 
<a class="small_text" href="#">What is this?</a>
</span>
<div id="tooltip">
<u>Contributor</u><br/>
If you're interested in browsing proposals, making contributions and spreading the word, you're a Contributor!
<br/><br/>
<u>Buyer</u><br/>
If you'll be submitting proposals and you're a Buyer!  Buyers are also able to browse and donate.  Unless you're planning on using LetBuyIt123 to raise money for your proposal, you should probably sign up as a contributor.
</div> 
<script>
$(document).ready(function() {
    $("#demo a").tooltip('#tooltip'); 
});
</script>

<div class="radio">
  <label><input type="radio" name="data[User][type]" value="donor">Contributor</label>
</div>
<div class="radio">
  <label><input type="radio" name="data[User][type]" value="scientist">Buyer</label>
</div>

<!-- 
<div class="multi"> 
	<div class="input radio"><input type="hidden" name="data[User][type]" id="UserType_" value="" />
		<input type="radio" name="data[User][type]" id="UserTypeDonor" value="donor"  class="switch-d"/>
		<label for="UserTypeDonor">Donor</label>
		<input type="radio" name="data[User][type]" id="UserTypeScientist" value="scientist" class="switch-s" />
		<label for="UserTypeScientist">Scientist</label>
	</div>

</div>
 -->	
	
	

 
<button id="search-button" class="btn btn-default">Register</button>

<?= $form->end() ?>
</dl>
<? } ?>

</div>
</div>

</form>
</div>

</div>
