<div class="container">

<div id="edit-form">

<div class="panel panel-default">
  <div class="panel-body" class="form-group">

		
		<h3>Existing Users: Log in</h3>
		<?php
		$session->flash('auth');
		echo $form->create('User', array('action' => 'login')); 
		?>

		<!-- <dl>
		<div class="field-hint-inactive" id="data[User][email]-H"><div><?=TT_USER_LOGIN_EMAIL?></div></div> -->
		<?= $form->input('email', array('class' => 'form-control', 'label' => 'Email')) ?>
		<!-- </dl> -->

		<!-- <dl>
		<div class="field-hint-inactive" id="data[User][password]-H"><div><?=TT_USER_LOGIN_PASSWORD?></div></div> -->
		<?= $form->input('password', array('class' => 'form-control', 'label' => 'Password')) ?>
		<span class="small_text help_text"><a href="<?= HTTP_BASE ?>users/reset_password">Forgot your password?</a> </span>
		<!-- </dl> -->

		<button id="search-button" class="btn btn-default">Login</button>

		<?= $form->end() ?>
		

	</div>
</div>


<div class="panel panel-default">
  	<div class="panel-body">

		<fieldset>
		<h3>New users: Join us!</h3>
		<div class="register">
		It's easy and it's free!  <?=$html->link('Sign up here', '/users/register')?>
		</div>
		</fieldset>
	
	</div>

</div>

</div>

</div>