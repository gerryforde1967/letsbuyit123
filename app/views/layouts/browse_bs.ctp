<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
    <!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
    <!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
    <!--[if IE 9]> <html class="no-js ie9 oldie" lang="en"> <![endif]-->
    <meta charset="utf-8">
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Coming soon, Bootstrap, Bootstrap 3.0, Free Coming Soon, free coming soon, free template, coming soon template, Html template, html template, html5, Code lab, codelab, codelab coming soon template, bootstrap coming soon template">
    <title>Bootstrap Templates</title>

    <?php
    echo $javascript->link( 'jquery.js' );
    echo $javascript->link( 'bootstrap.min.js' );
    echo $javascript->link( 'jquery.backstretch.js' );
	echo $javascript->link( 'FlexGauge.js' );
	?>
    
    <!-- ============ Google fonts ============ -->
    <link href='http://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' rel='stylesheet' type='text/css' />

    <?php
    echo $html->css( 'bootstrap.min.css' );
    echo $html->css( 'style.css' );
    echo $html->css( 'font-awesome.css' );
    echo $html->css('letsbuyit123.css');
    ?>

</head>
<body>
 <!-- C:\wamp\www\letsbuyit123\app\views\layouts\browse_bs.ctp -->
 
 <?php echo $this->element('header-main'); ?>
 
 <?php echo $content_for_layout;?>
 
 <?php //echo $this->element('footer'); ?>
 
 <?php //echo $cakeDebug; ?>

 <script type="text/javascript">
    'use strict';

    /* ========================== */
    /* ::::::: Backstrech ::::::: */
    /* ========================== */
    // You may also attach Backstretch to a block-level element
    $.backstretch(
        [
            /*
            "img/44.jpg",
            "img/colorful.jpg",
            "img/34.jpg",
            */
            //"img/images.jpg"
            '<?php echo HTTP_BASE . "img/images.jpg"; ?>'
        ],

        {
            //duration: 4500,
            //fade: 1500
        }
    );
 </script>
</body>
</html>