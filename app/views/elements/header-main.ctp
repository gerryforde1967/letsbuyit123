<div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container">
        <div class="navbar-header"><a class="navbar-brand" href="<?= Router::url('/', true) ?>">LetsBuyIt123</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav">                
            	<li>
            		<?= (empty( $logged_in_user['User']['id']) ? $html->link('Home', '/') : $html->link('My Account', '/users/profile')) ?>
            	</li>
        		<li>
        			<span class="left"></span><?php echo $html->link('Browse Projects', '/projects/category'); ?><span class="right"></span>
        		</li>
        		<li>
        			<?= $html->link('Help', '/pages/help') ?>
        		</li>
            </ul>

            <!--
            <form class="navbar-form navbar-left" id="SearchFindForm" method="post" action="/search/input">
		       	<div class="form-group">
			       	<fieldset style="display:none;">
			       		<input type="hidden" name="_method" value="POST" />
			       	</fieldset>
					<input type="text" class="form-control" value="<?php if(false) { echo 'search query string'; } else { echo 'Search'; } ?>" name="data[searchstr]" id="SearchSearchstr" onfocus="if(this.value=='Search')this.value='<?php echo ''; ?>'" onblur="if(this.value=='')this.value='Search'" onKeyPress="return submitenter(this,event)"/>
					<button id="search-button" class="btn btn-default">Submit</button>
				</div>
			</form>
			-->
			
			<ul class="nav navbar-nav navbar-right">
				<li>
					<?= (!empty( $logged_in_user['User']['id']) ? $html->link('Logout', '/users/logout') : $html->link('Login', '/users/login')) ?>
				</li>
				<?= (!empty( $logged_in_user['User']['id']) ? "" : "<li>" . $html->link('Donate', '/projects/donate/eurekafund') . "</li>") ?>
				<?= (!empty( $logged_in_user['User']['id']) && $logged_in_user['User']['privileges'] == 1 ? '<li>' . $html->link('Admin', '/admin/projects') . "</li>": '' ) ?>
			</ul>
	    </div>
    </div>
</div>