<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">LetsBuyIt123</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li> -->
        <li><?= (empty( $logged_in_user['User']['id']) ? $html->link('Home', '/') : $html->link('My Account', '/users/profile')) ?></li>
        <li><span class="left"></span><?php echo $html->link('Browse Projects', '/projects/category'); ?><span class="right"></span></li>
        <li><?= $html->link('Help', '/pages/help') ?></li>
       </ul>

	<form class="navbar-form navbar-left" id="SearchFindForm" method="post" action="/search/input">
       	<div class="form-group">
	       	<fieldset style="display:none;">
	       		<input type="hidden" name="_method" value="POST" />
	       	</fieldset>
			<input type="text" class="form-control" value="<?php if(false) { echo 'search query string'; } else { echo 'Search'; } ?>" name="data[searchstr]" id="SearchSearchstr" onfocus="if(this.value=='Search')this.value='<?php echo ''; ?>'" onblur="if(this.value=='')this.value='Search'" onKeyPress="return submitenter(this,event)"/>
			<button id="search-button" class="btn btn-default">Submit</button>
		</div>
	<?php echo $form->end();?>
		
	<!--
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    -->

      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="#">Link</a></li> -->
        <li><?= (!empty( $logged_in_user['User']['id']) ? $html->link('Logout', '/users/logout') : $html->link('Login', '/users/login')) ?></li>
			<?= (!empty( $logged_in_user['User']['id']) ? "" : "<li>" . $html->link('Donate', '/projects/donate/eurekafund') . "</li>") ?>
			<?= (!empty( $logged_in_user['User']['id']) && $logged_in_user['User']['privileges'] == 1 ? '<li>' . $html->link('Admin', '/admin/projects') . "</li>": '' ) ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!--
<div id="header">
		<div id="header-top" class="header-section">
		
			<div id="logo"><a href="<?=HTTP_BASE ?>"><img src="<?=HTTP_BASE ?>img/logo_beta_new.jpg" /></a>
			</div>
			
			<div id="top-right">
				<div id="top-links">
				<ul>
<li><?= (empty( $logged_in_user['User']['id']) ? $html->link('Home', '/') . " | " : $html->link('My Account', '/users/profile') . " | ") ?></li>
<li><?= (!empty( $logged_in_user['User']['id']) ? $html->link('Logout', '/users/logout') . " | " : $html->link('Login', '/users/login') . " | ") ?></li>
<?= (!empty( $logged_in_user['User']['id']) ? "" : "<li>" . $html->link('Donate', '/projects/donate/eurekafund') . "| </li>") ?>
<?= (!empty( $logged_in_user['User']['id']) && $logged_in_user['User']['privileges'] == 1 ? '<li>' . $html->link('Admin', '/admin/projects') . "| </li>": '' ) ?>
<li><?= $html->link('Help', '/pages/help') ?></li>
				</ul>
				</div>
				<div id="search">
					<table>
						<tr><td align="top">
						<form id="SearchFindForm" method="post" action="/search/input"><fieldset style="display:none;"><input type="hidden" name="_method" value="POST" /></fieldset>
						<input type="text" value="<?php if(false) { echo 'search query string'; } else { echo 'Search'; } ?>" name="data[searchstr]" id="SearchSearchstr" onfocus="if(this.value=='Search')this.value='<?php echo ''; ?>'" onblur="if(this.value=='')this.value='Search'" onKeyPress="return submitenter(this,event)"/></td><td align="top"><button id="search-button"></button></td></tr>
						<?php echo $form->end();?>
					</table>
				</div>
				<div class="clear"></div>
				
				<!--
				<div id="top-tabs"> 
					<ul> 
						<li><span class="left"></span><?php echo $html->link('browse projects', '/projects/category'); ?><span class="right"></span></li> 
						<li><span class="left"></span><?php echo $html->link('submit proposal', '/pages/project-intro'); ?><span class="right"></span></li>                      
					</ul> 
				</div>
				-> 
			
			</div>
		
		</div> <!-- header-top ->

		<div class="clear"></div>	
-->